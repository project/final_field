<?php

/**
 * @file
 * Contains final_field.module..
 */

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\field_ui\Form\FieldConfigEditForm;

/**
 * @defgroup final_field Final Field module
 * @ingroup final_field
 * @{
 */

/**
 * Implements hook_help().
 *
 * @param $route_name
 * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
 *
 * @return string
 */
function final_field_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the final_field module.
    case 'help.page.final_field':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Allows fields to be configured to take an initial value on entity creation that can no longer be changed once the entity is saved.') . '</p>';
      return $output;

    default:
      return NULL;
  }
}

/**
 * Implements hook_form_field_config_edit_form_alter().
 *
 * @param array $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * @param $form_id
 */
function final_field_form_field_config_edit_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  /** @var FieldConfigEditForm $form_object */
  $form_object = $form_state->getFormObject();
  /** @var \Drupal\field\FieldConfigInterface $field_config */
  $field_config = $form_object->getEntity();

  $form['third_party_settings']['final_field'] = [
    '#type' => 'details',
    '#title' => t('Final Field'),
    '#open' => TRUE,
  ];
  $form['third_party_settings']['final_field']['final_field'] = [
    '#type' => 'checkbox',
    '#title' => t('Disable for persistent entities'),
    '#description' => t('Enable the field only for new entities and disable it once an entity is saved.'),
    '#default_value' => $field_config->getThirdPartySetting('final_field', 'final_field', FALSE),
  ];

  $form['#validate'][] = '_final_field_inference_field_config_edit_form_validate';
}

/**
 * Validation handler for the field config form.
 *
 * @see final_field_form_field_config_edit_form_alter()
 *
 * @param array $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function _final_field_inference_field_config_edit_form_validate(array &$form, FormStateInterface $form_state) {
  // Just a stub
}

/**
 * Implements hook_form_alter().
 *
 * Make configured fields readonly.
 *
 * @param array $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * @param $form_id
 */
function final_field_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  /** @var EntityFormInterface $entity_form */
  if (!($entity_form = $form_state->getFormObject()) instanceof EntityFormInterface) {
    return;
  }
  /** @var ContentEntityInterface $entity */
  if (!($entity = $entity_form->getEntity()) instanceof ContentEntityInterface) {
    return;
  }

  // Ignore new entities
  if ($entity->isNew()) {
    return;
  }

  foreach ($entity->getFields() as $field_name => $field) {
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    if (!($field_config = $field->getFieldDefinition()) instanceof FieldConfigInterface) {
      continue;
    }

    // Disable configured final fields
    if ($field_config->getThirdPartySetting('final_field', 'final_field', FALSE)) {
      $form[$field_name]['#disabled'] = TRUE;
    }
  }

  $form['#after_build'][] = '_final_field_content_entity_form_after_build';
}

/**
 * After build handler for content entity forms.
 * Forces the default value for final fields.
 *
 * @param array $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *
 * @return array
 */
function _final_field_content_entity_form_after_build(array &$form, FormStateInterface $form_state) {
  /** @var EntityFormInterface $entity_form */
  $entity_form = $form_state->getFormObject();
  /** @var ContentEntityInterface $entity */
  $entity = $entity_form->getEntity();

  foreach ($entity->getFields() as $field_name => $field) {
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    if (!($field_config = $field->getFieldDefinition()) instanceof FieldConfigInterface) {
      continue;
    }

    // Force the current value for final fields
    if ($field_config->getThirdPartySetting('final_field', 'final_field', FALSE)) {
      if (isset($form[$field_name]['widget']['#default_value'])) {
        $form_state->setValue($field_name, $form[$field_name]['widget']['#default_value']);
      }
      elseif (isset($form[$field_name]['#default_value'])) {
        $form_state->setValue($field_name, $form[$field_name]['#default_value']);
      }
    }
  }

  return $form;
}

/** @} ingroup final_field */
